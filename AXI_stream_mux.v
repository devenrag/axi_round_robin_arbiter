// FILE NAME : Axi_stream_mux.v
// AUTHOR : Deven Rakeshkumar Gupta
// DATE : 4/01/2018

module AXI_stream_mux (
	input wire[31:0] A_tdata, 
	input A_tvalid, 
	input A_tlast,
	  
	input wire[31:0] B_tdata, 
	input B_tvalid, 
	input B_tlast,
	 
	  input [1:0]stream_sel,
	  
	output reg [31:0] k_tdata, 
	output reg k_tvalid, 
	output reg k_tlast);

	  always @ (*)
		begin
		  if (stream_sel == 2'b00)
		  {k_tdata,k_tvalid,k_tlast} = {32'bx,1'b0,1'b0};
		  else if (stream_sel == 2'b01)
		  {k_tdata,k_tvalid,k_tlast} = {A_tdata,A_tvalid,A_tlast};
		  else if (stream_sel == 2'b10)
		  {k_tdata,k_tvalid,k_tlast} = {B_tdata,B_tvalid,B_tlast};
		  else
		  {k_tdata,k_tvalid,k_tlast} = {32'bx,1'b0,1'b0};
		end
  
endmodule
