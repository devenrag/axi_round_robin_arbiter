// FILE NAME : rotating_prioritizer.v
// AUTHOR : Deven Rakeshkumar Gupta
// DATE : 4/01/2018

module rotating_prioritizer(
	input Clk,
	input Resetb,
	input SUGb,
	input wire[0:1] RQ,
	output reg[0:1] Final_SHR_Grant);
	/*
	SUGb :- SCU accepted and Utilized Grant (low active) i.e SCU has finished processing the current request. 
	Most_Recent_Read_Grant:- This registered information about the requester, who enjoyed grant recently helps to decide who is the least preferred requester for the next grant.
							 The outputs of this register act like one-hot select lines for the barrel shifters
	Read_GT:- Combinational Grant 
	Read_GT_R:- registered grants (held until the grant is fully utilized) 
	WR:- Output of input rotator (input of the Fixed Priority Resolver)
	WG:- Input of output rotator (Output of Fixed Priority Resolver)

	Final_SHR_Grant is registered and held until the grantee generates SUGb
	*/
	  reg [0:1] Most_Recent_Read_Grant, Read_GT, Read_GT_R, RR;  
	  wire [0:1] RG;
	wire ALORP; // ALORP = At Least One Request Present (active) = some raw (unregistered) grant is active
	reg update_bar_slash_hold;

	 
	//Fixed Priority Resolver for Read Grant
	assign RG[0] = RR[0]; 
	assign RG[1] = ~RR[0] & RR[1];  

	// ALORP = At Least One Request Present (active) 
	  assign ALORP = RQ[0] | RQ[1];
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Output Rotator for Read Grant
	always@(*) begin
			case(Most_Recent_Read_Grant)
			2'b10 : begin
			  Read_GT = {RG[1], RG[0]};
					end
			2'b01 : begin
			  Read_GT = {RG[0], RG[1]};
					end	
			default : begin
					Read_GT = RG;
			end
			endcase
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Input Rotator for Read Grant
			case(Most_Recent_Read_Grant)
			2'b10 : begin 
			  RR = {RQ[1], RQ[0]};
					end
			2'b01 : begin
			  RR = {RQ[0], RQ[1]};
					end	
			default : begin
			  RR = {RQ[0:1]};
			end
			endcase
	end
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	always@(*) begin
	 Final_SHR_Grant = Read_GT_R; // Read Grant registered
	end

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	always@(posedge Clk, negedge Resetb) 
	  begin
		if(Resetb == 0) 
		  begin
			Most_Recent_Read_Grant <= 2'b01; //initialised to 1
		  end
		else 
			begin
				if(SUGb == 0) 
					begin
						Most_Recent_Read_Grant <= Read_GT_R;
					end
			end
				
	end
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Here, we are coding the two-state state machine to control updating the output registered grant status (as shown in the state diagram figure attached)

	always@(posedge Clk, negedge Resetb) 
	  begin
		if(Resetb == 0) 
		   begin
			update_bar_slash_hold <= 1'b0; // to start with , go into idle state 
			// Also clear the registered grants
			Read_GT_R <= 2'b00; 
		   end
		else
		case (update_bar_slash_hold )
		1'b0: 
			begin 
			  Read_GT_R  <= Read_GT; 
			  if (ALORP) 	
				begin
				  update_bar_slash_hold <= 1'b1;
				end
			end
		1'b1:
			if (~SUGb) 	// on receiving low-going SUGb pulse, at the end of the clock we cancel all grants (meaning we inactivate the grant that was just utilized) 
						// and at the end of the subsequent clock (i.e. in the (update_bar_slash_hold == 0) state), we might potentially activate a new grant 
				begin
				  update_bar_slash_hold <= 1'b0;
				  Read_GT_R <= 2'b00; 
				end
		endcase
	  end
	
endmodule 


/////////