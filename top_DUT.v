// FILE NAME : top_DUT.v
// AUTHOR : Deven Rakeshkumar Gupta
// DATE : 4/01/2018

module top_DUT(
	input Clk,
	input Resetb,
	  
	input wire[31:0] A_tdata, 
	input A_tvalid, 
	input A_tlast,
	output A_tready,
	  
	input wire[31:0] B_tdata, 
	input B_tvalid, 
	input B_tlast,
	output B_tready,
	   
	output [31:0] k_tdata, 
	output k_tvalid, 
	output k_tlast,
	input k_tready);

	  //Internal Signals
	  wire ARQ , BRQ;
	  wire GTA , GTB;
	  wire [1:0] AXI_mux_sel;
	  
	  assign ARQ = A_tvalid & k_tready;
	  assign BRQ = B_tvalid & k_tready;
	  
	  rotating_prioritizer u_rotating_prioritizer (.Clk(Clk),
												   .Resetb(Resetb),
												   .SUGb(!(A_tlast | B_tlast)),
												   .RQ({ARQ,BRQ}),
												   .Final_SHR_Grant({GTA,GTB}));
	  
	  AXI_stream_sel_logic u_AXI_stream_sel_logic(	.Clk(Clk),
													.Resetb(Resetb),
													.Grant_A(GTA),
													.Grant_B(GTB),
													.AXI_mux_sel(AXI_mux_sel));
	  
	  
	  AXI_stream_mux u_AXI_stream_mux ( .A_tdata(A_tdata), 
										.A_tvalid(A_tvalid), 
										.A_tlast(A_tlast),
									  
										.B_tdata(B_tdata), 
										.B_tvalid(B_tvalid), 
										.B_tlast(B_tlast),
									 
										.stream_sel(AXI_mux_sel),
									  
										.k_tdata(k_tdata), 
										.k_tvalid(k_tvalid), 
										.k_tlast(k_tlast));
	  
	  AXI_stream_demux u_AXI_stream_demux ( .A_tready(A_tready), 
											.B_tready(B_tready),
											.stream_sel(AXI_mux_sel), 
											.k_tready(k_tready));
  
endmodule