// FILE NAME : AXI_stream_sel_logic.v
// AUTHOR : Deven Rakeshkumar Gupta
// DATE : 4/01/2018

module AXI_stream_sel_logic(
  input Clk,
  input Resetb,
  input Grant_A,
  input Grant_B,
  output reg [1:0] AXI_mux_sel);
  
  parameter VALID = 1'b0;
  parameter INVALID = 1'b1;
  
  reg state, next;
  reg [1:0] prev_AXI_mux_sel;
  
  always @ (posedge Clk or negedge Resetb) 
    begin
      if (!Resetb)
        state <= VALID;
      else
        state <= next;
    end
  
  always @ (Grant_A, Grant_B, state)
    begin
    //default values
      next = 1'bx;
      //AXI_mux_sel = 2'b0;
      case (state) 
          VALID : begin
            prev_AXI_mux_sel = AXI_mux_sel;
            if (Grant_A == Grant_B)
              next = INVALID;
            else if ((Grant_A) && (!Grant_B))
              begin // a=1 b=0
        		AXI_mux_sel = 2'b01;
            	next = VALID;
              end
     		 else if ((!Grant_A) && (Grant_B)) //a=0,b=1
               begin
       			 AXI_mux_sel = 2'b10;
             	 next = VALID;
               end
          end
          INVALID : begin
            AXI_mux_sel = prev_AXI_mux_sel;
            if (Grant_A == Grant_B)
              next = INVALID;
            else
              next = VALID;
          end
       endcase
  end

endmodule