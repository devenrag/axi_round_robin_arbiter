// FILE NAME : AXI_stream_demux.v
// AUTHOR : Deven Rakeshkumar Gupta
// DATE : 4/01/2018

module AXI_stream_demux (
	output reg A_tready, 
	output reg B_tready,
	input [1:0]stream_sel, 
	input  k_tready);
	  
	  always @ (*)
		begin
		  if (stream_sel == 2'b00)
		  {A_tready,B_tready} = {1'b0,1'b0};
		  else if (stream_sel == 2'b01)
		  {A_tready,B_tready} = {k_tready,1'b0};
		  else if (stream_sel == 2'b10)
		  {A_tready,B_tready} = {1'b0,k_tready};
		  else
		  {A_tready,B_tready} = {1'b0,1'b0};
		end
  
endmodule